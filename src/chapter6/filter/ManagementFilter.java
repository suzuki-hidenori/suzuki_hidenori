package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter({"/signup","/setting","/manegement"})
public class ManagementFilter implements Filter {

	public void init(FilterConfig config) {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		User user = (User) session.getAttribute("loginUser");

		int departmentId = user.getDepartmentId();
		int branchId = user.getBranchId();

		if (!(departmentId == 1 && branchId == 1)) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("権限がありません");
			session.setAttribute("errorMessages", errorMessages);

			((HttpServletResponse) response).sendRedirect("./");
			return;
		}

		chain.doFilter(request, response);// サーブレットを実行
	}

	public void destroy() {
	}

}
