package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/comments" })
public class CommentsServlet  extends HttpServlet {
	@Override
	   protected void doGet(HttpServletRequest request, HttpServletResponse response)
	         throws IOException, ServletException {

	        request.getRequestDispatcher("/home.jsp").forward(request, response);
	    }
	@Override
      protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        String text = request.getParameter("text");
        int messageId = Integer.valueOf(request.getParameter("messageId"));

        if (!isValid(text, errorMessages)) {
            session.setAttribute("errorMessages", errorMessages);
            response.sendRedirect("./");
            return;
        }

        Comment comment = new Comment();
        comment.setText(text);
        comment.setMessageId(messageId);

        User user = (User) session.getAttribute("loginUser");
        comment.setUserId(user.getId());

        new CommentService().insert(comment);
        response.sendRedirect("./");
    }

	private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isEmpty(text)) {
            errorMessages.add("コメントを入力してください");
        } else if (500 < text.length()) {
            errorMessages.add("500文字以下で入力してください");
        } else if ((text.replace("　"," ").replace(" ","").isEmpty())) {
        	errorMessages.add("コメントを入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
