package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Branch;
import chapter6.beans.Department;
import chapter6.beans.User;
import chapter6.service.BranchService;
import chapter6.service.DepartmentService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignupServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws IOException,ServletException{

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
	        throws IOException,ServletException{

		List<String> errorMessages = new ArrayList<String>();
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		User user = getUser(request);
		if (!isValid(user,errorMessages, request)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		}
		new UserService().insert(user);

		String account = request.getParameter("account");

		new UserService().duplicateAccount(account);
		response.sendRedirect("./");
	}
	private User getUser(HttpServletRequest request) throws IOException, ServletException {
		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setCheckPassword(request.getParameter("checkPassword"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.valueOf(request.getParameter("branchId")));
		user.setDepartmentId(Integer.valueOf(request.getParameter("departmentId")));
		return user;
	}
	private boolean isValid(User user, List<String> errorMessages, ServletRequest response) {
		String account = user.getAccount();
		String password = user.getPassword();
		String checkPassword = user.getCheckPassword();
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		User users = new UserService().duplicateAccount(account);

		((ServletRequest) response).setAttribute("users", users);

		if(StringUtils.isEmpty(account)) {
			errorMessages.add("アカウント名を入力してください");
		}else if(account.matches("[azAZ0*9]") || account.length() < 6) {
			errorMessages.add("アカウント名は６文字以上で入力してください");
		}else if(account.matches("[azAZ0*9]") || account.length() > 20) {
			errorMessages.add("アカウント名は２０文字以下で入力してください");
		}
		if(users != null) {
			errorMessages.add("アカウントが重複しています。");
		}
		if(StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if(password.matches("[azAZ0*9=/:=@\\[-~]") || password.length() < 6 ) {
			errorMessages.add("パスワードは半角６文字以上で入力してください");
		} else if(password.matches("[azAZ0*9=/:=@\\[-~]") || password.length() > 20) {
			errorMessages.add("パスワードは半角２０文字以下で入力してください");
		}else if(!(password.equals(checkPassword))) {
			errorMessages.add("確認用とパスワードが違います");
		}
		if(!StringUtils.isEmpty(name) && (10 < name.length())) {
			errorMessages.add("ユーザー名は10文字以下で入力してください");
		}else if(StringUtils.isEmpty(name)) {
			errorMessages.add("ユーザー名を入力してください");
		}
		if(branchId == 1 && departmentId >= 3) {
			errorMessages.add("支社と部署の組み合わせが違います。");
		} else if(branchId == 2 && departmentId <= 2) {
			errorMessages.add("支社と部署の組み合わせが違います。");
		} else if(branchId == 3 && departmentId <= 2) {
			errorMessages.add("支社と部署の組み合わせが違います。");
		} else if(branchId == 4 && departmentId <= 2) {
			errorMessages.add("支社と部署の組み合わせが違います。");
		}
		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
