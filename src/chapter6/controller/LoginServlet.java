package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;
import chapter6.beans.UserBranchDepartment;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
    	List<UserBranchDepartment> users = new UserService().select();

        request.setAttribute("users", users);

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String account= request.getParameter("account");
        String password = request.getParameter("password");
        User user = new UserService().select(account, password);

        List<String> errorMessages = new ArrayList<String>();

        if (account == null || account.isEmpty()  ){
            errorMessages.add("アカウントを入力してください");
        }
        if(user == null) {
        	errorMessages.add("アカウントまたはパスワードが間違っています。");
        }
        if (password.isEmpty() || password == null){
            errorMessages.add("パスワードを入力してください");
        }
        if(errorMessages.size() != 0) {
        	request.setAttribute("account", account);
        	request.setAttribute("password", password);
        	request.setAttribute("errorMessages", errorMessages);
        	request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        HttpSession session = request.getSession();

        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }
}