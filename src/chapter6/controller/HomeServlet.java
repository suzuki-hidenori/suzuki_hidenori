package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.Branch;
import chapter6.beans.Department;
import chapter6.beans.User;
import chapter6.beans.UserBranchDepartment;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.BranchService;
import chapter6.service.CommentService;
import chapter6.service.DepartmentService;
import chapter6.service.MessageService;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	String startDate = request.getParameter("startDate");
        String endData = request.getParameter("endData");
        String searchWord = request.getParameter("searchWord");


        List<UserBranchDepartment> users = (List<UserBranchDepartment>) new UserService().select();
    	List<UserMessage> messages = new MessageService().select(startDate, endData, searchWord);
        List<UserComment> comments = new CommentService().select();
		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();
		HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("loginUser");


        request.setAttribute("startDate", startDate);
        request.setAttribute("endData", endData);
        request.setAttribute("searchWord", searchWord);
        request.setAttribute("loginUser", loginUser);
		request.setAttribute("users", users);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
        request.setAttribute("comments", comments);
        request.setAttribute("messages", messages);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }
}
