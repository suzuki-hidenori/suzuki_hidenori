package chapter6.beans;

import java.util.Date;

public class UserComment {
	private int id;
    private int userId;
    private String account;
    
    private String text;
    private int messageId;
    
    private Date createdDate;
    private Date updatedDate;

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String[] getSplitedText() {
		return text.split("\n");
	}
	
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpDate() {
		return updatedDate;
	}
	public void setUpDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
