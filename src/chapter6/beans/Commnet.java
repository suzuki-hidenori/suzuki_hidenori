package chapter6.beans;

import java.util.Date;

public class Commnet {
	private int id;
    private int userId;
    private int messageId;
    private String text;
    private Date createdDate;
    private Date updatedDate;

    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpDate() {
		return updatedDate;
	}
	public void setUpDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
