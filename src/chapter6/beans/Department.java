package chapter6.beans;

public class Department {
	private String departmentName;
	private int id;
	
	public String getName() {
		return departmentName;
	}
	public void setName(String departmentName) {
		this.departmentName = departmentName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
