package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String startDate, String endData, String searchWord) {
		final int LIMIT_NUM = 1000;

		Date rowDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
		String date = dateFormat.format(rowDate);

		if (startDate == null || startDate.isEmpty()) {
			startDate = "2020-01-01 00:00:00";
		} else {
			startDate = startDate + " 00:00:00";
		}

		if (endData == null || endData.isEmpty()) {
			endData = date;
		} else {
			endData = endData + " 23:59:59";
		}

		if (searchWord == null || searchWord.isEmpty()) {
			searchWord = null;
		} else {
			searchWord = "%" + searchWord + "%";
		}
		Connection connection = null;
		try {
			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, startDate, endData, searchWord);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();

			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}