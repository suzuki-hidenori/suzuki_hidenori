<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <title>ユーザー登録</title>
</head>
    <body>
        <div class="main-contents">
            <c:if test="${not empty errorMessages}">
                <div class="errorMessages">
                    <ul class="validate">
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li ><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>



            <form action="signup" method="post" class="sigunup"><br />
            		<a href="manegement">ユーザー管理</a><br>

               		<label for="account">アカウント名</label>
                	<input name="account" id="account" value="${user.account}" />
               	 	<br /><br/>

                	<label for="password">パスワード</label>
                	<input name="password" type="password" id="password" />
                	<br/><br/>

                	<label for="checkPassword">確認用パスワード</label>
                	<input name="checkPassword" type="password" id="checkPassword" />
                	<br/><br/>

               	 	<label for="name">ユーザー名</label>
                	<input name="name" id="name" value="${user.name}" />
                	<br/><br/>

                	<label for="branchId">支社</label>
                	<select name="branchId" id="branchId">
                		<c:forEach items="${branches}" var="branch">
                			<c:if test="${branch.id == user.branchId}">
                				<option value="${branch.id }" selected>${branch.name}</option>
                			</c:if>
                			<c:if test="${branch.id != user.branchId}">
	                			<option value="${branch.id }">${branch.name}</option>
	                		</c:if>
                		</c:forEach>
                	</select>
                	<br/><br/>

                 	<label for="departmentId">部署</label>
                	<select name="departmentId" id="departmentId">
                		<c:forEach items="${departments}" var="department">
                			<c:if test="${department.id == user.departmentId}">
	                			<option value="${department.id }" selected>${department.name}</option>
	                		</c:if>
	                		<c:if test="${department.id != user.departmentId}">
	               				<option value="${department.id }">${department.name}</option>
	               			</c:if>
	               		</c:forEach>
                	</select>
                	<br/><br/>
                <input type="submit" value="登録" /> <br />
                <div class="copyright">Copyright(c)suzuki_hidenori</div>
            </form>
        </div>
    </body>
</html>