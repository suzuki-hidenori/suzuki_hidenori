<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="mainContent">
			<div class="link">
				<a href="message">新規投稿</a>
					<c:if test="${loginUser.branchId == 1}">
        				<a href="manegement">ユーザー管理</a>
        			</c:if>
        		<a href="logout">ログアウト</a><br/>
			</div>
        	<c:if test="${not empty errorMessages}">
    	        <div class="errorMessages">
       				<ul class="validate">
           				<c:forEach items="${errorMessages}" var="errorMessage">
               				<li><c:out value="${errorMessage}" />
           				</c:forEach>
        			</ul>
    			</div>
   				<c:remove var="errorMessages" scope="session" />
		   	</c:if>

			<form  action="home" method="get" class="dateCategory">
				日付
				<input type="date" name="startDate" value="${startDate}"><br>
				~
				<input type="date" name="endData" value="${endData}"><br>
				カテゴリ
				<input type="text" name="searchWord" value="${searchWord}"><br>
				<input type="submit" value="絞り込み">
			</form>

			<div class="messages">
    			<c:forEach items="${messages}" var="message">
        			<div class="message">

                			<span class="account"><c:out value="${message.account}" /></span><br><br>

                			<label for="title">タイトル：</label>
                			<span class="title"><c:out value="${message.title}" /></span><br><br>

                			<label for="category">カテゴリー：</label>
                			<span class="category"><c:out value="${message.category}" /></span><br><br>

							<label for="text">投稿内容：</label><br>
                			<c:forEach var="s" items="${message.splitedText}">
            					<div class="text"><c:out value="${s}" /></div>
            				</c:forEach>

							<br><br>
            				<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

            		
            			<c:if test="${loginUser.id == message.userId}">
            				<form action="deleteMessage" method="post" onsubmit="return check()">
            					<input type="hidden" name="messageId" value=${message.id}><br />
            					<input type="submit" value="メッセージ削除">
            				</form>
            			</c:if>
            		</div>
            		<script>
						function check(){
						    var result = window.confirm('このコメントを削除します。よろしいですか？');

						    if( result == false ) {
						 		return false;
						    }
						}
					</script>
            		<div class="comment">
        				<c:forEach items="${comments}" var="comment">
        				    <c:set var="id" value="${message.id}"></c:set>
        				   	<c:if test="${id == comment.messageId}">
                				<span class="account"><c:out value="${comment.account}" /></span><br>

                				<c:forEach var="s" items="${comment.splitedText}">
            						<div class="text"><c:out value="${s}" /></div>
            					</c:forEach>

								<br>
            					<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br>

            					<c:if test="${loginUser.id == comment.userId }">
            						<form action="deleteComment" method="post" onsubmit="return commentCheck()">
           			     				<input type="hidden" name="commentId" value=${comment.id}><br />
           								<input type="submit" value="コメント削除">
            						</form>
            					</c:if>
            				</c:if>
        			     </c:forEach>
        			</div>
        			<script>
						function commentCheck(){
						    var result = window.confirm('このコメントを削除します。よろしいですか？');

						    if( result == false ) {
						 		return false;
						    }
						}
					</script>
        				<form action="comments" method="post">
        			   		コメント<br />
        			   		<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
           					<input type="hidden" name="messageId" value="${message.id}"><br />
           					<input type="submit" value="つぶやく">
       				</form>
    			 </c:forEach>
    			 <div>Copyright(c)suzuki_hidenori</div>
			 </div>
			
        </div>
    </body> 
</html>