<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	<title>ユーザー管理画面</title>
</head>
	<body>
		<c:if test="${ not empty errorMessages }">
    		<div class="errorMessages">
        		<ul>
           			<c:forEach items="${errorMessages}" var="errorMessage">
            			<li><c:out value="${errorMessage}" />
           			</c:forEach>
       			</ul>
   			</div>
   			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<div class="mainContent">

			<div class="link">
				<a href="./">ホーム</a>
        		<a href="signup">ユーザー新規登録</a>
			</div>
			<div class="managementUser">
				<c:forEach items="${users}" var="user">
            		<div class="account-name">
              		  	<div class="account"><c:out value="${user.account}"></c:out></div>
            			<div class="name"><c:out value="${user.name}"></c:out></div>
             	 		<div class="branchId"><c:out value="${user.branchName}"></c:out></div>
            			<div class="departmentId"><c:out value="${user.departmentName}"></c:out></div>
             	 		<c:if test="${user.isStopped == 1}">
             	 			<div class="isStopped"><c:out value="停止中"></c:out></div>
           		   		</c:if>
             	 		<c:if test="${user.isStopped == 0}">
            	  			<div class="isStopped"><c:out value="復活"></c:out></div>
            	  		</c:if>

        			<form action="setting" method="get">
        				<input type="hidden" name="userId" value="${user.id}">
        				<input type="hidden" name="branchName" value="${user.branchName}">
        				<input type="hidden" name="departmentName" value="${user.departmentName}">
          		 		<input type="submit" value="編集">
         		   	</form>

					<c:if test="${loginUser.id != user.id}">
            			<form action="stop" method="post" onsubmit="return outage()" class="stopNumber">
            				<c:if test="${user.isStopped == 0}">
            					<input type="hidden" name="stopNumber" value="1">
            					<input type="hidden" name="userId" value= "${user.id}">
            					<input type="submit" value="停止" formaction="stop">
            				</c:if>
          		  		</form>
          	  			<form action="stop" method="post" onsubmit="return resurrection()">
							<c:if test="${user.isStopped == 1}">
								<input type="hidden" name="stopNumber" value="0">
								<input type="hidden" name="userId" value="${user.id}">
								<input type="submit" value="復活" formaction="stop">
							</c:if>
						</form>
					</c:if>
					</div>
					<script>
						function outage(){
							var result = window.confirm('このユーザーを停止しますか？');
				    		if( result == false ){
				 				return false;
							}
						}
						function resurrection(){
							var result = window.confirm('このユーザーを復活しますか？');
				    		if( result == false ){
				 				return false;
							}
						}
					</script>

    	   		</c:forEach>
     	  	</div>
       	</div>
		<div>Copyright(c)suzuki_hidenori</div>
    </body>
</html>