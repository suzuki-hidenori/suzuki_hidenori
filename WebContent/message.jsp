<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>Insert title here</title>
</head>
	<body>
		<c:if test="${not empty errorMessages}">
    		 <div class="errorMessages">
       			<ul class="validate">
            		<c:forEach items="${errorMessages}" var="errorMessage">
             			<li><c:out value="${errorMessage}" />
            		</c:forEach>
        		</ul>
   			</div>
   			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="form-area" >

        	<form action="message" method="post" class="messageForm">
        		<a href ="./">ホーム</a><br/>

 				<label for="title">件名</label>
            	<input name="title" id="title" value="${title}" /><br /><br/>

        		<label for="category">カテゴリ</label>
            	<input name="category" id="category" value="${category}" /><br /><br/>

           		投稿内容<br />
            	<textarea name="text" cols="100" rows="5" class="text-box">${text}</textarea><br />
            	<input type="submit" value="投稿">

				<div class="copyright">Copyright(c)suzuki_hidenori</div>
        	</form>
	 	</div>

	</body>
</html>