<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
    <body>
        <div class="main-contents">
        	<div class="errorMessage">
            	<c:if test="${ not empty errorMessages}">
    		   		<div class="errorMessages">
        				<ul class="validate">
            				<c:forEach items="${errorMessages}" var="errorMessage">
               					<li><c:out value="${errorMessage}" />
            				</c:forEach>
        				</ul>
    				</div>
   					<c:remove var="errorMessages" scope="session" />
				</c:if>
			</div>

            <form action="login" method="post" class="login-form"><br />

				<div class="account-form">
               		<label for="account">アカウント名かメールアドレス</label>
              		<input name="account" id="account" value="${account}"/> <br />
				</div>
				<div class="password-form">
               		<label for="password">パスワード</label>
               		<input name="password" type="password" id="password" value="${password}"/> <br />
				</div>
                <input type="submit" value="ログイン" class="submit-btn"/> <br />
                <div class="copyright">Copyright(c)suzuki_hidenori</div>
            </form>


        </div>
    </body>
</html>